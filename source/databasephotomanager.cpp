#include "stdafx.hpp"
#include "databasephotomanager.h"
#include "databasestuff.h"

DatabasePhotoManager::DatabasePhotoManager(const SharedPointer<DatabaseStuff> databasestuffin):
databasestuff(databasestuffin) {
	assert(nullptr != databasestuff.get());
}

// Returns the id if successfully inserted.
int64_t DatabasePhotoManager::insertPhotoData(const DatabasePhotoManager::PhotoData &photodata) {
	// Note: I was starting the transaction here, but instead, we start it before calling this method.
	//sqliteExec("BEGIN EXCLUSIVE TRANSACTION; ");
	static const char * querystring = "INSERT INTO photo (filenameoriginal, filename, year, month, tagtime, atime, ctime, mtime, checksum, filesize, filepathoriginal) VALUES(?,?,?,?,?,?,?,?,?,?,?); ";
	auto sqlstatement = databasestuff->sqlitePrepare(querystring);
	databasestuff->sqliteBind(sqlstatement, 1, photodata.filenameoriginal);
	databasestuff->sqliteBind(sqlstatement, 2, photodata.filename);
	databasestuff->sqliteBind(sqlstatement, 3, photodata.year);
	databasestuff->sqliteBind(sqlstatement, 4, photodata.month);
	databasestuff->sqliteBind(sqlstatement, 5, photodata.tagtime);
	databasestuff->sqliteBind(sqlstatement, 6, photodata.atime);
	databasestuff->sqliteBind(sqlstatement, 7, photodata.ctime);
	databasestuff->sqliteBind(sqlstatement, 8, photodata.mtime);
	databasestuff->sqliteBind(sqlstatement, 9, photodata.checksum);
	databasestuff->sqliteBind(sqlstatement, 10, photodata.filesize);
	databasestuff->sqliteBind(sqlstatement, 11, photodata.filepathoriginal);
	int rc = databasestuff->sqliteStep(sqlstatement);
	if (SQLITE_CONSTRAINT == rc) {
		return int64_t(DatabaseStuff::Errors::Constrant);
	}
	databasestuff->checkForError(rc, SQLITE_DONE);
	const int64_t databaseid = databasestuff->sqlite3LastInsertRowid();
	auto sqlstatement2 = databasestuff->sqlitePrepare(" SELECT id FROM string WHERE value = ?; ");
	auto sqlstatement3 = databasestuff->sqlitePrepare(" INSERT INTO string (value) VALUES(?); ");
	auto sqlstatement4 = databasestuff->sqlitePrepare(" INSERT INTO tag (section, name, value, photo) VALUES(?, ?, ?, ?); ");
	auto selectOrInsertString = [&](const String & theString) {
		databasestuff->sqliteReset(sqlstatement2);
		databasestuff->sqliteBind(sqlstatement2, 1, theString);
		rc = databasestuff->sqliteStep(sqlstatement2);
		int64_t rowId;
		if (SQLITE_ROW == rc) {
			rowId = databasestuff->sqliteColumnInt64(sqlstatement2, 0);
		} else {
			databasestuff->sqliteReset(sqlstatement3);
			databasestuff->sqliteBind(sqlstatement3, 1, theString);
			rc = databasestuff->sqliteStep(sqlstatement3);
			databasestuff->checkForError(rc, SQLITE_DONE);
			rowId = databasestuff->sqlite3LastInsertRowid();
		}
		return rowId;
	};
	for ( const auto & section: photodata.tags) {
		int64_t sectionId = selectOrInsertString(section.first);
		for ( const auto & tag: section.second) {
			int64_t tagNameId = selectOrInsertString(tag.first);
			int64_t tagValueId = selectOrInsertString(tag.second);
			databasestuff->sqliteReset(sqlstatement4);
			databasestuff->sqliteBind(sqlstatement4, 1, sectionId);
			databasestuff->sqliteBind(sqlstatement4, 2, tagNameId);
			databasestuff->sqliteBind(sqlstatement4, 3, tagValueId);
			databasestuff->sqliteBind(sqlstatement4, 4, databaseid);
			rc = databasestuff->sqliteStep(sqlstatement4);
			databasestuff->checkForError(rc, SQLITE_DONE);
		}
	}
	return databaseid;
}

Vector<Pair<int_fast16_t, int_fast8_t> > DatabasePhotoManager::getMonths() {
	Vector<Pair<int_fast16_t, int_fast8_t> > vector;
	auto statement = databasestuff->sqlitePrepare(" SELECT DISTINCT year, month FROM photo; ");
	while (SQLITE_ROW == databasestuff->sqliteStep(statement)) {
		vector.push_back ( Pair<int_fast16_t, int_fast8_t>(databasestuff->sqliteColumnInt(statement, 0), databasestuff->sqliteColumnInt(statement, 1)));
	}
	return vector;
}

Vector<DatabasePhotoManager::PhotoData> DatabasePhotoManager::getPhotosForMonth(Pair<int_fast16_t, int_fast8_t> month) {
	Vector<PhotoData> vector;
	auto statement = databasestuff->sqlitePrepare(" SELECT id, filename, tagtime FROM photo WHERE year = ? AND month = ? ORDER BY tagtime ASC, id ASC; ");
	databasestuff->sqliteBind(statement, 1, month.first);
	databasestuff->sqliteBind(statement, 2, month.second);
	while (SQLITE_ROW == databasestuff->sqliteStep(statement)) {
		PhotoData photodata;
		photodata.id = databasestuff->sqliteColumnInt64(statement, 0);
		photodata.filename = databasestuff->sqliteColumnText(statement, 1);
		photodata.tagtime = databasestuff->sqliteColumnInt64(statement, 2);
		vector.push_back(photodata);
		//sqliteColumnText
	}
	return vector;
}

Vector<DatabasePhotoManager::PhotoData> DatabasePhotoManager::getAllPhotos() {
	Vector<PhotoData> vector;
	auto statement = databasestuff->sqlitePrepare(" SELECT id, filename, tagtime, year, month FROM photo ORDER BY tagtime ASC, id ASC; ");
	while (SQLITE_ROW == databasestuff->sqliteStep(statement)) {
		PhotoData photodata;
		photodata.id = databasestuff->sqliteColumnInt64(statement, 0);
		photodata.filename = databasestuff->sqliteColumnText(statement, 1);
		photodata.tagtime = databasestuff->sqliteColumnInt64(statement, 2);
		photodata.year = databasestuff->sqliteColumnInt64(statement, 3);
		photodata.month = databasestuff->sqliteColumnInt64(statement, 4);
		vector.push_back(photodata);
		//sqliteColumnText
	}
	return vector;
}

