#include "stdafx.hpp"
#include "comparefilesduplicate.h"

static void openFile(const String &filename, MappedFileSource &filesource) {
	if (!filesource.is_open()) {
		filesource.open(filename);
		if (!filesource.is_open()) {
			throw new std::runtime_error(String("CheckFileDuplicate: failed to open file: ") + filename);
		}
	}
}

CompareFilesDuplicate::AreDuplicateEnum CompareFilesDuplicate::areDuplicate(const String &filenameA, const String &filenameB) {
	MappedFileSource sourceA, sourceB;
	openFile(filenameA, sourceA);
	openFile(filenameB, sourceB);
	return areDuplicate(sourceA, sourceB);
}

CompareFilesDuplicate::AreDuplicateEnum CompareFilesDuplicate::areDuplicate(const MappedFileSource &sourceA, const MappedFileSource &sourceB) {
	if (nullptr == sourceA.data() || nullptr == sourceB.data()) {
		throw new std::runtime_error(String("CheckFileDuplicate: failed to map file"));
	}
	if (sourceA.size() != sourceB.size()) {
		return AreDuplicateEnum::FilesAreDifferent;
	}
	if (0 == memcmp(sourceA.data(), sourceB.data(), sourceA.size())) {
		return AreDuplicateEnum::FilesAreDuplicate;
	}
	return AreDuplicateEnum::FilesAreDifferent;
}
