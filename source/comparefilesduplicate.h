#ifndef COMPAREFILESDUPLICATE_H
#define COMPAREFILESDUPLICATE_H

#include "stdafx.hpp"

class CompareFilesDuplicate
{
public:
	CompareFilesDuplicate() = default;
	CompareFilesDuplicate(const CompareFilesDuplicate&) = delete;
	CompareFilesDuplicate& operator=(const CompareFilesDuplicate&) = delete;
	CompareFilesDuplicate(const CompareFilesDuplicate&&) = delete;
	CompareFilesDuplicate& operator=(const CompareFilesDuplicate&&) = delete;  // end of implicit constructor deleting
	enum class AreDuplicateEnum {
		FilesAreDifferent,
		FilesAreDuplicate,
	};
	AreDuplicateEnum areDuplicate(const String &filenameA, const String &filenameB);
	AreDuplicateEnum areDuplicate(const MappedFileSource &sourceA, const MappedFileSource &sourceB);
};

#endif // COMPAREFILESDUPLICATE_H
