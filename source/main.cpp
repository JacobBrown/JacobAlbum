#include "stdafx.hpp"
#include "jacobalbum.h"
#include "databasestuff.h"
#include "databasephotomanager.h"

int main() {
	String todirectory("/tmp/JacobAlbum/Pictures");
	SharedPointer<DatabaseStuff> database(new DatabaseStuff( todirectory + "/test.db", true));
	SharedPointer<DatabasePhotoManager> databasePhotoManager(new DatabasePhotoManager(database));
	auto jacobalbum = JacobAlbum::create(todirectory, database, databasePhotoManager);
	//jacobalbum.getAllCrapFromDirectory("/var/www/incoming");
	//jacobalbum.generateAlbumHtmlAndJson();
	return EXIT_SUCCESS;
}
