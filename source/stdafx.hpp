#ifndef STDAFX_H
#define STDAFX_H
#if defined __cplusplus
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>
#include <stdio.h>
#include <sqlite3.h>
#include <stdexcept>
#include <string.h>

#include <libexif/exif-data.h>
#include "boost/date_time/posix_time/posix_time.hpp" //include all types plus i/o

#include <iterator>
#include <algorithm>
#include <assert.h>
#include <cstdio>
#include <cinttypes>
#include <openssl/sha.h>
#include <fstream>
#include <boost/algorithm/hex.hpp>
#if 0
#include <extractor.h> // Extractor doesn't have what we need
#endif
#include <taglib/tag.h>
#include <taglib/fileref.h>
#include <taglib/rifffile.h>
//#define _UNICODE
#define UNICODE
#include <MediaInfo/MediaInfo.h>
#undef UNICODE
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream_buffer.hpp>
#include <boost/iostreams/stream.hpp>

#include <locale>
#include <codecvt>
#include <tuple>

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
//#include <kcgi.h>
#include <unistd.h>

//#include <mstch/mstch.hpp>
#include <bustache/model.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>
//#include <rapidjson/rapidjson.h>
#include <iostream>

template<typename Key, typename Value> using UnorderedMap = std::unordered_map<Key, Value>;
using String = std::string;
template<typename Type, typename Deleter = std::default_delete<Type> > using UniquePointer = std::unique_ptr<Type, Deleter>;
template<typename Type> using SharedPointer = std::shared_ptr<Type>;
template<typename Type> using WeakPointer = std::weak_ptr<Type>;
using Path = boost::filesystem::path;
template<typename Type> using Vector = std::vector<Type>;
template<typename Type1, typename Type2> using Pair = std::pair<Type1, Type2>;
using MappedFileSource = boost::iostreams::mapped_file_source;

#endif //if defined __cplusplus
#endif // STDAFX_H
