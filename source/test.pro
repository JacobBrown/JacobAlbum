CONFIG -= qt
DEFINES -= UNICODE QT_LARGEFILE_SUPPORT

SOURCES += jacobalbum.cpp \
    databasestuff.cpp \
    main.cpp \
    jacobfile.cpp \
    ../dependencies/bustache/src/generate.cpp \
    ../dependencies/bustache/src/format.cpp

INCLUDEPATH += ../kcgi/include
#INCLUDEPATH += mstch/include
#INCLUDEPATH += ../dependencies/../mustach
INCLUDEPATH += ../dependencies/bustache/include
INCLUDEPATH += ../dependencies/rapidjson-1.1.0/include

#LIBS += -L ../kcgi/lib -lkcgi
LIBS += -lz
LIBS += -lboost_filesystem
LIBS += -lboost_system
LIBS += -lexif
LIBS += -lboost_date_time
LIBS += -lsqlite3
LIBS += -lcrypto
#LIBS += -lextractor
#LIBS += -ltag
LIBS += -lmediainfo
LIBS += -lboost_iostreams

#stuff for test:
LIBS += -lboost_regex

HEADERS += \
    jacobalbum.h \
    databasestuff.h \
    jacobfile.h \
    stdafx.hpp

PRECOMPILED_HEADER = stdafx.hpp
CONFIG += precompile_header

CONFIG   += c++11
