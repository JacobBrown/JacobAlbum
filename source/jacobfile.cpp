#include "stdafx.hpp"
#include "jacobfile.h"
#include "jacobalbum.h"

JacobFile::JacobFile(SharedPointer<DatabaseStuff> database, SharedPointer<JacobAlbum> jacobAlbumIn, SharedPointer<DatabasePhotoManager> databasePhotoManager) {
	this->jacobAlbum = jacobAlbumIn;
	this->database = database;
	this->databasePhotoManager = databasePhotoManager;
}

bool JacobFile::moveIntoAlbum(const boost::filesystem::path & pathIn, const boost::filesystem::path & albumpath) {
	currentpath = pathIn;
	currentpathstring = currentpath.string();
	photodata.filenameoriginal = currentpath.filename().string();
	photodata.filepathoriginal = currentpathstring;
	photodata.filesize = int64_t(boost::filesystem::file_size(currentpath));
	if (! getTimestampFromFileSystem()) {
		return false;
	}
	getTimestampFromLibExif();
	getTimestampFromMediaInfoLib();
	if (0 == photodata.tagtime) {  // We couldn't get the timestamp from any of our methods.
		return false;
	}
	if (! calculateSha1Checksum()) {
		return false;
	}
	auto transaction = database->beginTransaction();
	photodata.filename = currentpath.filename().string();
	int64_t newid = databasePhotoManager->insertPhotoData(toPhotoData());
	if (newid <= 0) { // TODO: We need to work with filename already exist errors.
		return false;
	}
	if (! moveToAlbumDirectory(albumpath)) {
		return false;
	}
	transaction->commitTransaction();
	/* TODO:  There might be a chance that the transaction fails to commit because of a conflict.
	 * If that is true, we need to check the for success and if failure, move the file back.
	 * I wish more filesystems supported transactions, but it looks like it's mostly a MS Windows feature for now. */
	return true;
}

bool JacobFile::getTimestampFromLibExif() {
	String filepath = currentpathstring;
	std::unique_ptr<ExifData, void(*)(ExifData*)> exifdata(exif_data_new_from_file(filepath.c_str()), exif_data_unref);
	if (!exifdata) {
		return false;
	}
	for (int ifdnumber = 0 ; ifdnumber < EXIF_IFD_COUNT; ifdnumber++) {
		auto lambda = [&](ExifEntry *entry) {
			char buffer[1024];
			String value = exif_entry_get_value(entry, buffer, sizeof(buffer));
			String tag = exif_tag_get_name_in_ifd(entry->tag, ExifIfd(ifdnumber));
			photodata.tags["Exif"][tag] = value;
			if (EXIF_TAG_DATE_TIME == entry->tag) {
				parseDateTime(value, "%Y:%m:%d %H:%M:%S");
			}
		};
		exif_content_foreach_entry(exifdata->ifd[ifdnumber], [](ExifEntry *entry, void *userdata) 	{
			(*static_cast<decltype(lambda)*>(userdata))(entry);
		}, &lambda);
	}
	return true; // TODO: ?
}

bool JacobFile::getTimestampFromMediaInfoLib() {
	try {
		MediaInfoLib::MediaInfo mediainfo;
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		MediaInfoLib::String mediastring(converter.from_bytes(currentpathstring));
		mediainfo.Open(mediastring);
		mediainfo.Option(__T("Output"), __T("XML"));
		auto inform = mediainfo.Inform();
		String informstring(converter.to_bytes(inform));
		std::cout << "Inform string: " << informstring << std::endl ;
		boost::property_tree::ptree ptree;
		boost::iostreams::stream<boost::iostreams::array_source> stream(informstring.c_str(), informstring.size());  // TODO: Can we just use the UTF16 string?
		std::stringstream stream2(informstring);
		boost::property_tree::read_xml(stream2, ptree, boost::property_tree::xml_parser::trim_whitespace);
#if 1
		for (boost::property_tree::ptree::value_type & child: ptree) {
#if 1
			for (boost::property_tree::ptree::value_type & child2: child.second) {
				String string1;
				for (boost::property_tree::ptree::value_type & child3: child2.second) {
					const String & string2 = child3.first.data();
					if (string2 == "<xmlattr>") {
						for (boost::property_tree::ptree::value_type & child4: child3.second) {
							if (((String)child4.first.data()) == "type" ) { // TODO: Fix casting
								string1 = child4.second.data();
								break;
							}
						}
						continue;
					}
					const String & string3 = child3.second.data();
					photodata.tags[string1][string2] = string3;
					if ( string1 == "General" && string2 == "Mastered_date" ) {
						parseDateTime(string3, "%Y-%m-%d %H:%M:%S");
					}
				}

			}
#endif
		}
#endif
		if (true) { // TODO: This is an example of creating JSON with boost property_tree
			boost::property_tree::ptree ptree2(ptree);
			std::ostringstream oss;
			boost::property_tree::json_parser::write_json(oss, ptree2);
			String jsonString = oss.str();
			std::cout << "ptree as JSON:\n" << jsonString << "\n\n";
			fflush(stdout);
		}
		if (true) {
			for ( auto one: photodata.tags) {
				for ( auto two: one.second) {
					std::cout << "Blah: " << one.first << ", " << two.first << ", " << two.second << "\n";
				}
			}
		}
		return true;
	} catch (boost::property_tree::json_parser::json_parser_error e) { // TODO: We'll have to move this too.
		//TODO: Log error message?
	} catch (boost::property_tree::xml_parser::xml_parser_error e) {
		//TODO: Log error message?
	}
	return false;
}

bool JacobFile::getTimestampFromFileSystem() {
	std::time_t time = boost::filesystem::last_write_time(currentpath);
	photodata.mtime = time;
	return true;
}

bool JacobFile::calculateSha1Checksum() {
	SHA_CTX context;
	String digeststring;
	digeststring.resize(SHA_DIGEST_LENGTH);
	boost::filesystem::ifstream filestream;
	char buffer[1024 * 16] ; // Note: SHA1 actually uses 512 bit blocks, so we should use a multiple of that.
	filestream.open(currentpath, std::ifstream::in);
	if(!SHA1_Init(&context)) {
		return false;
	}
	int64_t totallengthread = 0;
	while (filestream.good())  {
		filestream.read(buffer, sizeof(buffer));
		int64_t lengthread = filestream.gcount();
		totallengthread += lengthread;
		if(!SHA1_Update(&context, buffer, size_t(lengthread))) {
			return false;
		}
	}
	assert(totallengthread == photodata.filesize);
	if (totallengthread != photodata.filesize) {
		return false; // The size of the file in the filesystem is different from the ammount that we just did checksum on
	}
	if(!SHA1_Final(const_cast<unsigned char *>( reinterpret_cast<const unsigned char * > (digeststring.c_str())), &context)) {
		return false;
	}
	photodata.checksum = boost::algorithm::hex(digeststring);
	return true;
}

bool JacobFile::moveToAlbumDirectory(Path albumpath) {
	try {
#if 0
		albumpath.append(std::to_string(photodata.year));
		boost::filesystem::create_directory(albumpath);
		albumpath.append(std::to_string(photodata.month));
		boost::filesystem::create_directory(albumpath);
#endif
		albumpath = JacobAlbum::albumPathForMonth(albumpath, photodata.year, photodata.month);
		albumpath.append(photodata.filename);
		boost::filesystem::rename(currentpath, albumpath);
		currentpath = albumpath;
		currentpathstring = currentpath.string();
		return true;
	} catch ( boost::filesystem::filesystem_error error) {
		std::cout << " | " <<  error.path1() << " | " <<  error.path2() << " | " <<  error.what() << " | " << error.code() << "\n";
	}
	// TODO: Finish this functionnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
	// What else do we need to do?
	// Check Work around file name collisions.
	return false;
}

DatabasePhotoManager::PhotoData JacobFile::toPhotoData() const {
	return photodata;
}

bool JacobFile::parseDateTime(const String &stringToParse, const char *pattern) {
	// TODO: Is this really the best way we can parse the date/time ?
	boost::posix_time::ptime ptime;
	std::istringstream is(stringToParse);
	is.imbue(std::locale(std::locale::classic(), new boost::posix_time::time_input_facet(pattern)));
	is >> ptime;
	if (ptime != boost::posix_time::ptime()) {
		photodata.month = ptime.date().month();
		photodata.year = ptime.date().year();
		photodata.tagtime = boost::posix_time::to_time_t(ptime);
		return true;
	}
	return false;
}
