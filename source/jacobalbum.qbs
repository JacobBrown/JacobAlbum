import qbs

Project {
  minimumQbsVersion: "1.6"
  CppApplication {
    name: "JacobAlbum"
    files: [
        "comparefilesduplicate.cpp",
        "comparefilesduplicate.h",
        "databasephotomanager.cpp",
        "databasephotomanager.h",
        "jacobalbum.h",
        "jacobalbum.cpp",
        "databasestuff.h",
        "databasestuff.cpp",
        "main.cpp",
        "jacobfile.h",
        "jacobfile.cpp",
    ]
    Group {
      prefix: "../dependencies/bustache/src/"
      files: [
        "generate.cpp",
        "format.cpp"
      ]
    }
    cpp.includePaths: [
      "../dependencies/bustache/include",
      "../dependencies/rapidjson-1.1.0/include"
    ]
    cpp.dynamicLibraries: [
      "z",
      "boost_filesystem",
      "boost_system",
      "exif",
      "boost_date_time",
      "sqlite3",
      "crypto",
      "mediainfo",
      "boost_regex",
      "boost_iostreams"
    ]
    cpp.cxxLanguageVersion: "c++14"
    Group {
      files: ["stdafx.hpp"]
      fileTags: ["cpp_pch_src"]
    }
  }
}
