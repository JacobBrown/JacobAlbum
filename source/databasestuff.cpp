#include "stdafx.hpp"
#include "databasestuff.h"

DatabaseStuff::Transaction::Transaction(DatabaseStuff *databasestuffIn) {
	databasestuff = databasestuffIn;
	databasestuff->sqliteExec("BEGIN EXCLUSIVE TRANSACTION; ");

}

DatabaseStuff::Transaction::~Transaction() {
	if (!done) {
		rollbackTransaction();
	}
}

void DatabaseStuff::Transaction::commitTransaction() {
	assert (!done);
	if (!done) {
		databasestuff->sqliteExec("COMMIT TRANSACTION; ");
		done = true;
	}
}

void DatabaseStuff::Transaction::rollbackTransaction() {
	assert (!done);
	if (!done) {
		databasestuff->sqliteExec("ROLLBACK TRANSACTION; ");
		done = true;
	}
}


DatabaseStuff::DatabaseStuff(const String & fileName, bool createIfNotExist) {
	initDatabase(fileName, createIfNotExist);
}

DatabaseStuff::~DatabaseStuff() {
	assert (nullptr != db);
	if (nullptr != db) {
		sqlite3_close(db);
		db = nullptr;
	}
}

void DatabaseStuff::initDatabase(const String & fileName, bool createIfNotExist) {
	int returncode;
	assert(nullptr == db);
	returncode = sqlite3_open_v2(fileName.c_str(), &db, SQLITE_OPEN_READWRITE | ( createIfNotExist ? SQLITE_OPEN_CREATE : 0 ) , nullptr); // don't create!
	if (returncode != SQLITE_OK) {
		throw std::runtime_error("Couldn't open database.");
	}
	sqliteExec(" PRAGMA journal_mode = WAL; ");
	sqliteExec(" PRAGMA synchronous = NORMAL; ");

	// Do all the database initialization in a single transaction
	sqliteExec(" BEGIN EXCLUSIVE TRANSACTION; ");
	if (createIfNotExist) {
		sqliteExec(" CREATE TABLE IF NOT EXISTS schemaversion (id INTEGER PRIMARY KEY AUTOINCREMENT, value INTEGER); ");
		sqliteExec(" INSERT INTO schemaversion (id, value) VALUES(1, 0); ", false);
	}
	auto statement = sqlitePrepare("SELECT value FROM schemaversion WHERE id = 1; ", true);
	int schemaversion = sqliteColumnInt(statement, 0);
	switch(schemaversion) {
		case 0:
			sqliteExec(" CREATE TABLE photo ( "
								 "  id INTEGER PRIMARY KEY AUTOINCREMENT, "
								 "  filenameoriginal TEXT, "
								 "  filename TEXT, "
								 "  checksum TEXT, "
								 "  year INTEGER, "
								 "  month INTEGER, "
								 "  tagtime INTEGER, "
								 "  atime INTEGER, "
								 "  ctime INTEGER, "
								 "  mtime INTEGER, "
								 "  filesize INTEGER, "
								 "  filepathoriginal TEXT "
								 " ); ");
			sqliteExec(" CREATE UNIQUE INDEX uniquePhotoFilenamePerDirectory ON photo (filename, month, year); " );
		case 1:
			sqliteExec(" CREATE TABLE tag ( "
								 "  id INTEGER PRIMARY KEY, "
								 "  section INTEGER, "
								 "  name INTEGER, "
								 "  value INTEGER, "
								 "  photo INTEGER "
								 " ); ");
			sqliteExec(" CREATE TABLE string ( "
								 "  id INTEGER PRIMARY KEY, "
								 "  value TEXT UNIQUE "
								 " ); ");
			sqliteExec(" UPDATE schemaversion SET value = 2 WHERE id = 1; ");
			sqliteExec(" COMMIT TRANSACTION; ");
			break;
		case 2:
			sqliteExec(" ROLLBACK TRANSACTION; ");
			break;
		default:
			throw std::runtime_error("database is newer than us");
	}
}

void DatabaseStuff::sqliteExec(const char *sql, bool doThrowOnError) {
	char * errormessage = nullptr;
	int rc = sqlite3_exec(db, sql, nullptr, nullptr, &errormessage);
	String errorstring;
	if (nullptr != errormessage) {
		errorstring = errormessage;
		sqlite3_free(errormessage);
	}
	if (rc != SQLITE_OK && doThrowOnError) {
		throw std::runtime_error(String("SQL exec failed. Query:\n") + sql + "\n\nerrormessage:\n" + errorstring);
	}
}

UniquePointer<DatabaseStuff::Transaction> DatabaseStuff::beginTransaction() {
	return UniquePointer<Transaction>(new Transaction(this));
}

UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)>  DatabaseStuff::sqlitePrepare(const char *sqlStatement, bool stepFirstRow) {
	sqlite3_stmt * sqlstatement = nullptr;
	int rc = sqlite3_prepare_v2(db, sqlStatement, -1, &sqlstatement, nullptr);
	if (nullptr == sqlstatement) {
		throw std::runtime_error(String("SQL prepare failed. sqlstatement is null.  Query:\n") + sqlStatement + "\n\nerrormessage:\n" + sqlite3_errmsg(db));
	}
	UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> sqlstatementunique(sqlstatement, sqlite3_finalize);
	if (rc != SQLITE_OK) {
		throw std::runtime_error(String("SQL prepare failed. Query:\n") + sqlStatement + "\n\nerrormessage:\n" + sqlite3_errmsg(db) );
	}
	if (stepFirstRow) {
		rc = sqlite3_step(sqlstatement);
		if (SQLITE_ROW != rc) {
			throw std::runtime_error(String("No row returned.\n"));
		}
	}
	return sqlstatementunique;
}

void DatabaseStuff::checkForError(int rc, int expectedRc, const char * errormessage) {
	if (rc != expectedRc) {
		throw std::runtime_error(String(errormessage) + sqlite3_errmsg(db) );
	}
}

void DatabaseStuff::sqliteBind(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number, const String &string, const int expectedRc) {
	const int rc = sqlite3_bind_text(statement.get(), number, string.c_str(), int(string.size()), SQLITE_STATIC);
	checkForError(rc, expectedRc, "SQL bind failed.\n\nerrormessage:\n");
}

void DatabaseStuff::sqliteBind(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number, const int integer, const int expectedRc) {
	const int rc = sqlite3_bind_int(statement.get(), number, integer);
	checkForError(rc, expectedRc, "SQL bind failed.\n\nerrormessage:\n");
}

void DatabaseStuff::sqliteBind(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number, const int64_t integer, const int expectedRc) {
	const int rc = sqlite3_bind_int64(statement.get(), number, integer);
	checkForError(rc, expectedRc, "SQL bind failed.\n\nerrormessage:\n");
}

int DatabaseStuff::sqliteStep(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement) {
	return sqlite3_step(statement.get());
}

void DatabaseStuff::sqliteReset(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, bool clearBindingsToo) {
	int rc = sqlite3_reset(statement.get());
	checkForError(rc, SQLITE_OK, "SQL reset failed.\n\nerrormessage:\n");
	if (!clearBindingsToo) {
		return;
	}
	rc = sqlite3_clear_bindings(statement.get());
	checkForError(rc, SQLITE_OK, "SQL clear bindings failed.\n\nerrormessage:\n");
}

int DatabaseStuff::sqliteColumnInt(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number) {
	assert(sqlite3_column_count(statement.get()) > number);  // assert that the column number is within the column count of statement.  It is undefined if we go out.
	return sqlite3_column_int(statement.get(), number);
}

String DatabaseStuff::sqliteColumnText(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number) {
	assert(sqlite3_column_count(statement.get()) > number);  // assert that the column number is within the column count of statement.  It is undefined if we go out.
	return reinterpret_cast<const char*>(sqlite3_column_text(statement.get(), number)); // Note: I hate that I have to cast this.  It makes me feel like this might fail in some UTF8 cases that we should test for.
}

int64_t DatabaseStuff::sqliteColumnInt64(UniquePointer<sqlite3_stmt, int (*)(sqlite3_stmt *)> &statement, const int number) {
	assert(sqlite3_column_count(statement.get()) > number);  // assert that the column number is within the column count of statement.  It is undefined if we go out.
	return sqlite3_column_int64(statement.get(), number);
}

int64_t DatabaseStuff::sqlite3LastInsertRowid() {
	return sqlite3_last_insert_rowid(db);
}
