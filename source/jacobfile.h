#ifndef JACOBFILE_H
#define JACOBFILE_H

#include "stdafx.hpp"
#include "databasephotomanager.h"
#include "databasestuff.h"

class JacobAlbum;

class JacobFile {

public:
	JacobFile(SharedPointer<DatabaseStuff> database, SharedPointer<JacobAlbum> jacobAlbum, SharedPointer<DatabasePhotoManager> databasePhotoManager);
	//bool moveIntoAlbum(const String &originalIn);
	bool moveIntoAlbum(const Path & pathIn, const Path & albumpath);

private:
	bool getTimestampFromLibExif();
	bool getTimestampFromMediaInfoLib();
	bool getTimestampFromFileSystem();
	bool calculateSha1Checksum();
	bool moveToAlbumDirectory(Path albumpath);
	DatabasePhotoManager::PhotoData toPhotoData() const;
	bool parseDateTime(const String & stringToParse, const char * pattern);

	boost::filesystem::path currentpath;
	String currentpathstring;
	DatabasePhotoManager::PhotoData photodata;
	SharedPointer<DatabasePhotoManager> databasePhotoManager;
	SharedPointer<DatabaseStuff> database;
	SharedPointer<JacobAlbum> jacobAlbum;
};

#endif // JACOBFILE_H
