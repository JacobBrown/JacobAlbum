#ifndef JACOBALBUM_H
#define JACOBALBUM_H

#include "stdafx.hpp"

class DatabaseStuff;
class DatabasePhotoManager;

// TODO: Why do I need to inherit with public?  Why won't private work?
class JacobAlbum: public std::enable_shared_from_this<JacobAlbum> {

private:
	JacobAlbum(const String &todirectory, SharedPointer<DatabaseStuff> databasestuff, SharedPointer<DatabasePhotoManager> databasePhotoManager);

public:
	~JacobAlbum();
	JacobAlbum() = delete;   // prevent implicit constructors (and operators?)
	JacobAlbum(const JacobAlbum&) = delete;
	JacobAlbum& operator=(const JacobAlbum&) = delete;
	JacobAlbum(const JacobAlbum&&) = delete;
	JacobAlbum& operator=(const JacobAlbum&&) = delete;  // end of implicit constructor deleting
	
	static SharedPointer<JacobAlbum> create(const String &todirectory, SharedPointer<DatabaseStuff> databasestuff, SharedPointer<DatabasePhotoManager> databasePhotoManager);
	void getAllCrapFromDirectory(const String & fromdirectory);
	void generateAlbumHtmlAndJson();
	//static void getTimestampFromExifFile(String filepath);
	static Path albumPathForMonth(Path albumpath, int_fast16_t year, int_fast16_t month);
	static String httpAlbumPathForMonth(int_fast16_t year, int_fast16_t month);

private:
	const String todirectory;
	const SharedPointer<DatabaseStuff> databasestuff;
	const SharedPointer<DatabasePhotoManager> databasePhotoManager;

};

#endif // JACOBALBUM_H
