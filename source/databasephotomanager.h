#ifndef DATABASEPHOTOMANAGER_H
#define DATABASEPHOTOMANAGER_H

class DatabaseStuff;

class DatabasePhotoManager
{
public:

	struct PhotoData {
		int64_t id = 0;
		String filenameoriginal;
		String filename;
		String checksum;
		int_fast16_t year = 0;
		int_fast16_t month = 0;
		int64_t tagtime = 0; // EXIF_TAG_DATE_TIME as time_t
		int64_t atime = 0;  // a,c,mtime's from before they were moved to album
		int64_t ctime = 0;
		int64_t mtime = 0;
		int64_t filesize = 0;
		String filepathoriginal;
		UnorderedMap<String, UnorderedMap< String, String> > tags; // metadata tags .. exif & others
	};

	DatabasePhotoManager(const SharedPointer<DatabaseStuff> databasestuff);
	// Returns the id if successfully inserted.
	int64_t insertPhotoData(const DatabasePhotoManager::PhotoData &photodata);
	Vector<Pair<int_fast16_t, int_fast8_t> > getMonths();
	Vector<DatabasePhotoManager::PhotoData> getPhotosForMonth(Pair<int_fast16_t, int_fast8_t> month);
	Vector<DatabasePhotoManager::PhotoData> getAllPhotos();

private:
	const SharedPointer<DatabaseStuff> databasestuff;
};

#endif // DATABASEPHOTOMANAGER_H
