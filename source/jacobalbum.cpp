#include "stdafx.hpp"
#include "jacobalbum.h"
#include "databasestuff.h"
#include "databasephotomanager.h"
#include "jacobfile.h"

JacobAlbum::JacobAlbum(const String & todirectoryIn, SharedPointer<DatabaseStuff> databasestuffIn, SharedPointer<DatabasePhotoManager> databasePhotoManagerIn) :
	todirectory(todirectoryIn), databasestuff(databasestuffIn), databasePhotoManager(databasePhotoManagerIn) {
}

JacobAlbum::~JacobAlbum() {
}

SharedPointer<JacobAlbum> JacobAlbum::create(const String &todirectory, SharedPointer<DatabaseStuff> databasestuff, SharedPointer<DatabasePhotoManager> databasePhotoManager) {
	return SharedPointer<JacobAlbum>(new JacobAlbum(todirectory, databasestuff, databasePhotoManager));
}

void JacobAlbum::getAllCrapFromDirectory(const String & fromdirectory) {
	Path frompath(fromdirectory);
	Path topath(todirectory);
	if (!boost::filesystem::exists(topath) || !boost::filesystem::exists(frompath) ) {
		// TODO: Error
		return;
	}
	for (Path::iterator iterator(frompath.begin()), iteratorend(frompath.end()); iterator != iteratorend; ++iterator) {
		printf("iterator: %s\n", iterator->string().c_str());
		fflush(stdout);
	}
	int filecount = 0;
	boost::filesystem::recursive_directory_iterator iterator(frompath);
	while (iterator != boost::filesystem::recursive_directory_iterator() ) {
		auto path = (*iterator).path();
		iterator++;  // Iterate before we rename to avoid race condition which throws exception
		if (! boost::filesystem::is_regular_file(path)) {
			continue;
		}
		std::cout << '\n' << path << '\n';
		JacobFile jacobfile(databasestuff, shared_from_this(), databasePhotoManager);
		jacobfile.moveIntoAlbum(path, todirectory);
		filecount++;
	}
	// TODO: Logging
	printf("filecount: %d\n", filecount);
}

void JacobAlbum::generateAlbumHtmlAndJson() {
	{  // First, All photos
		auto photos = databasePhotoManager->getAllPhotos();
		boost::iostreams::mapped_file_source singlephotoMustacheHtml(Path(todirectory).append("..").append("singlephoto.mustache.html"));
		// TODO: Make a month template
		boost::iostreams::mapped_file_source monthPhotosMustacheHtml(Path(todirectory).append("..").append("allphotos.mustache.html"));
		bustache::format monthformat(monthPhotosMustacheHtml);
		bustache::array allPhotosBustacheArray;
		bustache::array monthPhotosBustacheArray;
		decltype(photos[0].year) year = 0;
		decltype(photos[0].month) month = 0;
		for (decltype(photos.size()) i = 0; i < photos.size(); i++) {
			if ((photos[i].year != year || photos[i].month != month)) {
				if (!monthPhotosBustacheArray.empty()) {
					// We just finished a month, now make the album for it before we start the next one;
					bustache::object photoBustacheData {
						{"photos", monthPhotosBustacheArray},
						{"base", "../../"},
					};
					boost::filesystem::ofstream ofstream(albumPathForMonth(Path(todirectory), year, month).append("index.html")) ;
					ofstream << monthformat(photoBustacheData);
					// Clear our month variables to get ready for the next
					monthPhotosBustacheArray.clear();
				}
				month = photos[i].month;
				year = photos[i].year;
			}
			bustache::object photoBustacheData {
				{"id", photos[i].filename },
				{"filename", photos[i].filename },
				{"filenamePath", httpAlbumPathForMonth (photos[i].year, photos[i].month) + "/" + photos[i].filename },
				{"year", photos[i].filename },
				{"month", photos[i].filename },
			};
			#if 0
			boost::property_tree::ptree ptree {
				{"year", photos[i].filename },
			};
			#endif
			{ // Make HTML for single photo
				bustache::format format(singlephotoMustacheHtml);
				boost::filesystem::ofstream ofstream(albumPathForMonth(todirectory, photos[i].year, photos[i].month).append(photos[i].filename + ".html")) ;
				ofstream << format(photoBustacheData);
			}
			// TODO: Make JSON for single photo
			// TODO: Add to JSON array for all photoes
			allPhotosBustacheArray.push_back(photoBustacheData);
			monthPhotosBustacheArray.push_back(photoBustacheData);
		}
		{ // Make the big HTML with all them photos.
			bustache::object photoBustacheData {
				{"photos", allPhotosBustacheArray},
				{"base", ""},
			};
			boost::iostreams::mapped_file_source allphotosMustacheHtml(Path(todirectory).append("..").append("allphotos.mustache.html"));
			bustache::format format(allphotosMustacheHtml);
			boost::filesystem::ofstream ofstream(Path(todirectory).append("AllPhotos.html")) ;
			ofstream << format(photoBustacheData);
		}
	}

	return;
	auto monthAlbums = databasePhotoManager->getMonths();
	for (auto month: monthAlbums) {
		auto photos = databasePhotoManager->getPhotosForMonth(month);

	}
}

Path JacobAlbum::albumPathForMonth(Path albumpath, int_fast16_t year, int_fast16_t month) {
	albumpath.append(std::to_string(year));
	boost::filesystem::create_directory(albumpath);
	if (month >= 10) {
		albumpath.append(std::to_string(month));
	} else {  // Zero padding the month.  eg "01" instead of "1"
		albumpath.append(String("0") + std::to_string(month));
	}
	boost::filesystem::create_directory(albumpath);
	return albumpath;
}

String JacobAlbum::httpAlbumPathForMonth(int_fast16_t year, int_fast16_t month) {
	if (month >= 10) {
		return std::to_string(year) + "/" + std::to_string(month) ;
	}
	return std::to_string(year) + "/0" + std::to_string(month) ;
}
