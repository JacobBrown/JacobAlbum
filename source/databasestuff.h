#ifndef DATABASESTUFF_H
#define DATABASESTUFF_H

#include "stdafx.hpp"

struct sqlite3;
typedef struct sqlite3 sqlite3;
struct sqlite3_stmt;
typedef struct sqlite3_stmt sqlite3_stmt;

class DatabaseStuff {

public:

	enum class Errors {
		Constrant = -13
	};

	class Transaction {
	public:
		Transaction(DatabaseStuff * databasestuffIn);
		~Transaction();
		void commitTransaction();
		void rollbackTransaction();
	private:
		DatabaseStuff * databasestuff;
		bool done = false;
	};

	DatabaseStuff(const String & fileName, bool createIfNotExist = false);
	~DatabaseStuff();
	DatabaseStuff() = delete;   // prevent implicit constructors (and operators?)
	DatabaseStuff(const DatabaseStuff&) = delete;
	DatabaseStuff& operator=(const DatabaseStuff&) = delete;
	DatabaseStuff(const DatabaseStuff&&) = delete;
	DatabaseStuff& operator=(const DatabaseStuff&&) = delete;  // end of implicit constructor deleting
	void sqliteExec(const char *sql, bool doThrowOnError = true);
	UniquePointer<Transaction> beginTransaction();
	UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> sqlitePrepare(const char *sql, bool stepFirstRow = false);
	void sqliteBind(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number, const String & string, const int expectedRc = SQLITE_OK);
	void sqliteBind(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number, const int integer, const int expectedRc = SQLITE_OK);
	void sqliteBind(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number, const int64_t integer, const int expectedRc = SQLITE_OK);
	int sqliteStep(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement);
	void sqliteReset(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, bool clearBindingsToo = true);
	int sqliteColumnInt(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number = 0);
	String sqliteColumnText(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number = 0);
	int64_t sqliteColumnInt64(UniquePointer<sqlite3_stmt, int(*)(sqlite3_stmt*)> &statement, const int number = 0);
	int64_t sqlite3LastInsertRowid();
	void checkForError(int rc, int expectedRc, const char * errormessage = "Error!\n");

private:
	void initDatabase(const String & fileName, bool createIfNotExist);
	sqlite3 *db = nullptr;

};

#endif // DATABASESTUFF_H
