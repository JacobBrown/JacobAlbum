#include "../source/stdafx.hpp"
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../source/comparefilesduplicate.h"

namespace {

	class CompareFilesDuplicateTest : public ::testing::Test {
	protected:

		void SetUp() override {
			fileNameA = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
			fileNameB = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
		}

		void createA(const int bufferSize = 1000, const int repeatCount = 1000) {
			boost::filesystem::ofstream streamA(fileNameA);
			auto buffer = std::make_unique<char[]>(bufferSize);
			for (int i = 0 ; i < bufferSize; i++) {
				buffer[i] = rand();
			}
			for (int i = 0 ; i < repeatCount; i++) {
				streamA.write(buffer.get(), bufferSize);
			}
			streamA.close();
		}

		void copyAToB() {
			boost::filesystem::copy(fileNameA, fileNameB);
		}

		void truncateBByOne() {
			boost::filesystem::resize_file(fileNameB, boost::filesystem::file_size(fileNameB) - 1);
		}

		void flipLastByteOfB() {
			boost::iostreams::mapped_file file(fileNameB);
			file.data()[file.size() - 1] ^= 0xff;
			file.close();
		}

		void TearDown() override {
			boost::filesystem::remove(fileNameA);
			boost::filesystem::remove(fileNameB);
		}

		Path fileNameA;
		Path fileNameB;
	};

	TEST_F(CompareFilesDuplicateTest, Duplicate) {
		// Arrange
		this->createA();
		this->copyAToB();
		// Act
		CompareFilesDuplicate comparefilesduplicate;
		auto areDuplicate = comparefilesduplicate.areDuplicate(this->fileNameA.string(), this->fileNameB.string());
		// Assert
		EXPECT_EQ(areDuplicate, CompareFilesDuplicate::AreDuplicateEnum::FilesAreDuplicate);
	}

	TEST_F(CompareFilesDuplicateTest, DifferentSize) {
		// Arrange
		this->createA();
		this->copyAToB();
		this->truncateBByOne();
		// Act
		CompareFilesDuplicate comparefilesduplicate;
		auto areDuplicate = comparefilesduplicate.areDuplicate(this->fileNameA.string(), this->fileNameB.string());
		// Assert
		EXPECT_EQ(areDuplicate, CompareFilesDuplicate::AreDuplicateEnum::FilesAreDifferent);
	}

	TEST_F(CompareFilesDuplicateTest, DifferentData) {
		// Arrange
		this->createA();
		this->copyAToB();
		this->flipLastByteOfB();
		// Act
		CompareFilesDuplicate comparefilesduplicate;
		auto areDuplicate = comparefilesduplicate.areDuplicate(this->fileNameA.string(), this->fileNameB.string());
		// Assert
		EXPECT_EQ(areDuplicate, CompareFilesDuplicate::AreDuplicateEnum::FilesAreDifferent);
	}

}
